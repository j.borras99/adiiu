/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var llest;
var int_act;
var is_actor;
var llista_autors;
var autor_pelis = [];
var obj = [];

function getParPerNom(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


$(document).ready(function () {
    llest = false;
    is_actor = false;
    $("#fitxa_actor").hide();
    agafarActors();
    int_act = getParPerNom("persona")
    if (int_act != -1) {
        console.log(int_act);
        Espera();
        agafarNumPelis();
    }

});

function omplirFitxa() {
    $("#nom_act").html(llista_autors.todosporedad[int_act].vals[0]);
    $("#neix_act").html(llista_autors.todosporedad[int_act].vals[1]);
    $("#mor_act").html(llista_autors.todosporedad[int_act].vals[2]);
    $("#tpelis_act").html(autor_pelis[autor_pelis.length - 1].t_pelis);
    $("#fitxa_actor").show();
}

//Procediment que accedeix a la BD per a recollira la quantitat de pelicules d'un actor i calcular el seu porcentatge respecte a la resta d'actors vists.
function agafarNumPelis() {
    var i = 0;
    var total_pelis;
    //Mirarem si ja hem accedit a algun actor, en cas afirmatiu, agafarem la llista guardada en caché.
    if (sessionStorage.getItem("autor_pelis") != null) {
        autor_pelis = JSON.parse(sessionStorage.getItem("autor_pelis"));
console.log(autor_pelis);

        while (i < autor_pelis.length && !is_actor) {
            if (autor_pelis[i].actor === int_act) {
                console.log("YES")
                is_actor = true;
            }
            i++;
        }
        // En cas contrari guardarem que en total tenim 0 pelicules sumades. (aixo nomes s'executarà el primer pic que accedeixi aqui)
    } else {
        sessionStorage.setItem("total_pelis", 0);
    }
    if (!is_actor) {
        $.ajax({url: "http://localhost:8080/build/bdpeliculas?op=pelisdepersona&par=" + llista_autors.todosporedad[int_act].vals[0],
            success: function (result) {
                autor_pelis.push({actor: int_act, t_pelis: JSON.parse(result).pelisdepersona.length, p_pelis: 0});
                //Feim parseInt de l'objecte que ens retorna la memoria de cache ja que sino no ho agafa com a Integer
                sessionStorage.setItem("total_pelis", parseInt(sessionStorage.getItem("total_pelis")) + JSON.parse(result).pelisdepersona.length);
                // Un cop hem agafat el nou actor, calculem el % de pelis respecte tots els que tenim ja en memoria.
                for (var i = 0; i < autor_pelis.length; i++) {
                    autor_pelis[i].p_pelis = (autor_pelis[i].t_pelis / parseInt(sessionStorage.getItem("total_pelis")) * 100).toFixed(2);
                }
                sessionStorage.setItem("autor_pelis", JSON.stringify(autor_pelis));
                console.log(autor_pelis);
                omplirFitxa();
                prepararDades();
                llest = true;
                pintarGrafica();
            }
        });
    } else {
        omplirFitxa();
        prepararDades();
        llest = true;
        pintarGrafica();
    }
}
function Espera() {
    $("#espera").append('<img src="../imatges/loading.gif" style="width:500px ; height: 375px">');
}

function pintarGrafica() {
    if (llest) {

        $("#espera").empty();
        pie();
    }
}

function prepararDades() {
    //llista_autors = JSON.parse(sessionStorage.getItem("nom_actors"));
    for (var i = 0; i < autor_pelis.length; i++) {
        obj.push({name: llista_autors.todosporedad[autor_pelis[i].actor].vals[0] + " (" + autor_pelis[i].t_pelis + ")", y: parseInt(autor_pelis[i].p_pelis), color: getColorPattern(i), website: "actor.jsp?persona=" + autor_pelis[i].actor});
    }
    console.log(obj);

}
// Procediment que accedeix a la BD per a collir els 10 primers actors.
function agafarActors() {
    llista_autors = JSON.parse(sessionStorage.getItem("nom_actors"));

    if (llista_autors == null) {
        $.ajax({url: "http://localhost:8080/build/bdpeliculas?op=todosporedad&par=",
            success: function (result) {
                sessionStorage.setItem("nom_actors", result);
                llista_autors = JSON.parse(result);
                ferParaulesNuvol();
            }
        });
    } else {
        ferParaulesNuvol();
    }

}

// Funció encarregada de crear el nuvol de paraules.
function ferParaulesNuvol() {
    //Eliminar gift d'espera

    //Inserir el nom dels actors dintre de la llista.
    console.log(llista_autors);
    for (var i = 0; i < llista_autors.todosporedad.length; i++) {
        $("#ll_autors").append('<li><a href="actor.jsp?persona=' + i + '">' + llista_autors.todosporedad[i].vals[0] + '</a></li>');
    }

    //Crear nuvol de paraules.
    if (!$('#myCanvasActors').tagcanvas({
        textColour: '#000000',
        outlineThickness: 1,
        outlineColour: '#000000',
        maxSpeed: 0.03,
        depth: 0.75
    }, 'tags')) {
        $('#myCanvasContainer').hide();
    }

}

//Funcions necessaries per a realitzar el Pie Chart.
//Funció per a collir el color amb el que omplir el tros del Pie chart.
function getColorPattern(i) {
    var colors = Highcharts.getOptions().colors,
            patternColors = [colors[2], colors[0], colors[3], colors[1], colors[4]],
            patterns = [
                'M 0 0 L 5 5 M 4.5 -0.5 L 5.5 0.5 M -0.5 4.5 L 0.5 5.5',
                'M 0 5 L 5 0 M -0.5 0.5 L 0.5 -0.5 M 4.5 5.5 L 5.5 4.5',
                'M 1.5 0 L 1.5 5 M 4 0 L 4 5',
                'M 0 1.5 L 5 1.5 M 0 4 L 5 4',
                'M 0 1.5 L 2.5 1.5 L 2.5 0 M 2.5 5 L 2.5 3.5 L 5 3.5'
            ];

    return {
        pattern: {
            path: patterns[i],
            color: patternColors[i],
            width: 5,
            height: 5
        }
    };
}

//Funció que dibuixa el Pie Chart
function pie() {
    Highcharts.chart('container_actor', {
        chart: {
            type: 'pie'
        },
        title: {
            text: '% de pel·lícules per Actors.'
        },
        subtitle: {
            text: 'Es mostraran aquells actors que han sigut seleccionats'
        },
        tooltip: {
            valueSuffix: '%',
            borderColor: '#8ae'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    connectorColor: '#777',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                },
                point: {
                    events: {
                        click: function () {
                            window.location.href = this.website;
                        }
                    }
                },
                cursor: 'pointer',
                borderWidth: 3
            }
        },
        series: [{
                name: 'Percentatge',
                data: obj
            }],
        responsive: {
            rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        plotOptions: {
                            series: {
                                dataLabels: {
                                    format: '<b>{point.name}</b>'
                                }
                            }
                        }
                    }
                }]
        }
    });
}


