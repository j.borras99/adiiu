<%-- 
    Document   : login
    Created on : 21-oct-2020, 18:16:07
    Author     : dmiltim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Log In</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link rel="stylesheet" href="css/login.css">


        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

        <script src="JS/Blowfish.js"></script> 
        <script src="JS/connectDB.js"></script> 
    </head>
    <body>

        <%@include file ="capcalera.jsp" %>

        <!--<h1>Login</h1>
        
            <label>Username: </label><input type="text" id="name_user" name="name"><br>
            <label>Password: </label><input type="password" id="psswrd_user" name="psswrd"><br>
            <button id="login"> Log in</button>
        -->

        <!-- contenido -->
        <div class="container login mt-5">
            <div class="container">
                <div class="row grid-divider">
                    <div class="col-md p-5 ml-4">
                        <div class="h3">
                            Entra ahora
                        </div>
                        <hr class="solid"> 
                        <div class="h5">
                            Hola! Que tal? Registrate! Son solo 10 segundo!
                            No te arrepentirás!
                            <br>
                            <br>

                            <a class="btn btn-lg btn-primary btn-block mt-4" role="button" href="<%= request.getContextPath()%>/register.jsp">Registrarse</a>
                        </div>
                    </div>

                    <div class="vl"></div>
                    <div class="col-md  m-4  p-4 text-center ">
                        <div class="form-group ">
                            <form id="loginInfo" class="form-signin insput-sm" action="entrada.jsp" method="POST">
                                <input type="text" name="op" value="login" hidden>
                                <i class="fas fa-sign-in-alt"></i>
                                <h1 class="h3 mb-3 font-weight-normal">Identifícate</h1>
                                <%
                                    Object error = session.getAttribute("login_error");
                                    //Si s'ha definit la variable de error i hi ha hagut algun error mostra el missatge de error
                                    if ((error != null) && ((Boolean) error)) {
                                        session.setAttribute("login_error", false);
                                %>
                                <div class="alert alert-danger" role="alert">
                                    Error al intentar acceder a tu cuenta. Vuelve a intentarlo.
                                </div>
                                <%
                                    }
                                %>
                                <label for="inputName" class="sr-only">Nombre</label>
                                <input type="text" name="name_user" id="name_user" class="form-control input-sm my-1" placeholder="Nombre de usuario"
                                       required autofocus>
                                <label for="inputPassword" class="sr-only">Password</label>
                                <input type="password" id="psswrd_user" class="form-control my-1" placeholder="Contraseña"
                                       onkeypress="encrypt()"required>
                                <input type ="password" name="psswrd_user_encr" id="psswrd_user_encr" hidden>
                                <button type="submit" class="btn btn-lg btn-primary btn-block mt-2" id="login">Identificarse</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </body>
</html>
