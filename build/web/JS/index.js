/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var pelis_amb_valoracio = [];
var pelis_num_valoracions = [];
var distribucio_valoracions = new Array(11).fill(0);
var ratings_pelis;
var localidades = [];
var localidades_mostrar = [];
var vots_pelis = [];




$(document).ready(function () {
    prepararMap();
    agafarPelisRandom();
});

function prepararMap() {

    $.ajax({url: "http://localhost:8080/build/bdpeliculas?op=poblaciones&par=",
        success: function (result) {

            localidades = JSON.parse(result);
            console.log(localidades);

            crearCheckbox(localidades);
            map(localidades_mostrar);
        }
    });

}

function map(localidades) {
    console.log("localidades:");
    console.log(localidades);
    Highcharts.mapChart('container_3', {
        chart: {
            map: 'countries/es/es-all',
            backgroundColor: 'transparent'
        },
        title: {
            text: 'Highmaps basic lat/lon demo'
        },
        mapNavigation: {
            enabled: true
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '<b>{point.name}</b><br>Lat: {point.lat}, Lon: {point.lon}'
        },
        series: [{
                // Use the gb-all map with no data as a basemap
                name: 'Basemap',
                borderColor: '#A0A0A0',
                nullColor: 'rgba(200, 200, 200, 0.3)',
                showInLegend: false
            }, {
                name: 'Separators',
                type: 'mapline',
                nullColor: '#707070',
                showInLegend: false,
                enableMouseTracking: false
            }, {
                // Specify points using lat/lon
                type: 'mappoint',
                name: 'Cities',
                color: Highcharts.getOptions().colors[1],
                data: localidades

            }]
    });
}

function crearCheckbox(localidades) {
    var obj = localidades.poblacions;
    for (i = 0; i < obj.length; i++) {
        $('#lista-poblaciones').append('<div class="checkbox align-middle"><input class="checkbox" type="checkbox" value="' + i + '"> ' + obj[i].name + ' </div>');
    }

    $(".checkbox").change(function () {
        var i = $(this).val();
        console.log("hola");
        if (this.checked) {
            localidades_mostrar[localidades_mostrar.length] = localidades.poblacions[i];

        } else {
            localidades_mostrar = localidades_mostrar.filter(function (localidad) {
                return localidad != localidades.poblacions[i];
            });
        }

        map(localidades_mostrar);
    });
}



function agafarPelisRandom() {
    var lp;
    $.ajax({url: "http://localhost:8080/build/bdpeliculas?op=pelisrandom&par=",
        beforeSend: function () {
            $('#loading').html("<img src='imatges/loading.gif'>");
        },
        success: function (result) {           
            lp = JSON.parse(result);
            console.log(lp);
            return agafarRatingsPelis(lp);
        }
    });

}

function agafarRatingsPelis(lp) {
    //Preparem una cadena amb el id de les pelicules
    var aux = "{";
    for (var i = 0; i < lp.llista_pelis.length; i++) {
        aux += '"' + lp.llista_pelis[i].tconst + '",';
    }
    aux = aux.substring(0, aux.length - 1); //Ultima coma
    aux += "}";
    //Feim la petició a la base de dades
    var res;
    $.ajax({url: "http://localhost:8080/build/bdpeliculas?op=ratinglistapelis&par=" + aux,
        success: function (result) {
            res = JSON.parse(result);
            console.log(res);
            prepararDades(res);
            return res;
        }
    });

}

//Procediment que accedeix a la BD per a recollira la quantitat de vots d'una pelicula i calcular el seu porcentatge respecte a la resta
function agafarVotesDePelis() {
    var i = 0;
    var total_vots;

    $.ajax({url: "http://localhost:8080/build/bdpeliculas?op=pelisdepersona&par=" + llista_autors.todosporedad[int_act].vals[0],
        success: function (result) {
            autor_pelis.push({actor: int_act, t_pelis: JSON.parse(result).pelisdepersona.length, p_pelis: 0});
            //Feim parseInt de l'objecte que ens retorna la memoria de cache ja que sino no ho agafa com a Integer
            sessionStorage.setItem("total_pelis", parseInt(sessionStorage.getItem("total_pelis")) + JSON.parse(result).pelisdepersona.length);
            // Un cop hem agafat el nou actor, calculem el % de pelis respecte tots els que tenim ja en memoria.
            for (var i = 0; i < autor_pelis.length; i++) {
                autor_pelis[i].p_pelis = (autor_pelis[i].t_pelis / parseInt(sessionStorage.getItem("total_pelis")) * 100).toFixed(2);
            }
            sessionStorage.setItem("autor_pelis", JSON.stringify(autor_pelis));
            console.log(autor_pelis);
            omplirFitxa();
            prepararDades();
            llest = true;
            pintarGrafica();
        }
    });


    prepararDades();
    llest = true;
    pintarGrafica();
}



function prepararDades(data) {
    data = data.peliculasconrating;
    var numvotes = 0;
    console.log(data);
    for (var i = 0; i < data.length; i++) {
        pelis_amb_valoracio[i] = [data[i].title, data[i].rating];
        pelis_num_valoracions[i] = {name: data[i].title, vots: data[i].votes, y: null, color: getColorPattern(i)};
        distribucio_valoracions[Math.floor(data[i].rating)] = distribucio_valoracions[Math.floor(data[i].rating)] + 1;
        numvotes += data[i].votes;
    }
    
    $('#loading').html("");
    dibuixarColumnes();
    dibuixarDistribucio();

    for (var i = 0; i < pelis_num_valoracions.length; i++) {
        pelis_num_valoracions[i].y = Number((pelis_num_valoracions[i].vots / numvotes * 100).toFixed(2));
    }
    dibuixarTarta();

}

function dibuixarColumnes() {
    console.log(pelis_amb_valoracio);
    Highcharts.chart('container_1', {
        chart: {
            type: 'column',
            backgroundColor: 'transparent'
        },
        title: {
            text: 'Rating of each film (0 - 10)'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Score (from 0 to 10)'
            }
        },
        series: [{
                name: "puntuació",
                data: pelis_amb_valoracio
            }]
    });
}

function dibuixarDistribucio() {
    console.log(distribucio_valoracions);
    Highcharts.chart('container_4', {
        chart: {
            type: 'bar',
            backgroundColor: 'transparent'
        },
        title: {
            text: 'Distribució de les notes asignades'
        },
        xAxis: {
            categories: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Distribució de notes',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
                name: 'pelis',
                data: distribucio_valoracions
            }]
    });
}

function dibuixarTarta() {
    console.log(pelis_num_valoracions);
    Highcharts.chart('container_2', {
        chart: {
            type: 'pie',
            backgroundColor: 'transparent'
        },
        title: {
            text: '% de vots per pel·lícula.'
        },
        tooltip: {
            valueSuffix: '%',
            borderColor: '#8ae'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    connectorColor: '#777',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                },
                point: {
                    events: {
                        
                    }
                },
                cursor: 'pointer',
                borderWidth: 3
            }
        },
        series: [{
                name: 'Percentatge',
                data: pelis_num_valoracions
            }],
        responsive: {
            rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        plotOptions: {
                            series: {
                                dataLabels: {
                                    format: '<b>{point.vots}</b>'
                                }
                            }
                        }
                    }
                }]
        }
    });
}


//Funcions necessaries per a realitzar el Pie Chart.
//Funció per a collir el color amb el que omplir el tros del Pie chart.
function getColorPattern(i) {
    var colors = Highcharts.getOptions().colors,
            patternColors = [colors[2], colors[0], colors[3], colors[1], colors[4]],
            patterns = [
                'M 0 0 L 5 5 M 4.5 -0.5 L 5.5 0.5 M -0.5 4.5 L 0.5 5.5',
                'M 0 5 L 5 0 M -0.5 0.5 L 0.5 -0.5 M 4.5 5.5 L 5.5 4.5',
                'M 1.5 0 L 1.5 5 M 4 0 L 4 5',
                'M 0 1.5 L 5 1.5 M 0 4 L 5 4',
                'M 0 1.5 L 2.5 1.5 L 2.5 0 M 2.5 5 L 2.5 3.5 L 5 3.5'
            ];

    return {
        pattern: {
            path: patterns[i % 5],
            color: patternColors[Math.floor(Math.random() * 10)%5],
            width: 5,
            height: 5
        }
    };
}

//Funció que dibuixa el Pie Chart
function pie() {
    Highcharts.chart('container_actor', {
        chart: {
            type: 'pie'
        },
        title: {
            text: '% de pel·lícules per Actors.'
        },
        subtitle: {
            text: 'Es mostraran aquells actors que han sigut seleccionats'
        },
        tooltip: {
            valueSuffix: '%',
            borderColor: '#8ae'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    connectorColor: '#777',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                },
                point: {
                    events: {
                        click: function () {
                            window.location.href = this.website;
                        }
                    }
                },
                cursor: 'pointer',
                borderWidth: 3
            }
        },
        series: [{
                name: 'Percentatge',
                data: obj
            }],
        responsive: {
            rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        plotOptions: {
                            series: {
                                dataLabels: {
                                    format: '<b>{point.name}</b>'
                                }
                            }
                        }
                    }
                }]
        }
    });
}

