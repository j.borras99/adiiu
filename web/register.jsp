<%-- 
    Document   : login
    Created on : 21-oct-2020, 18:16:07
    Author     : dmiltim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrarse</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
              integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Techange - Registration</title>
        <link rel="icon" href="img/logotechange.ico">

        <script src="https://kit.fontawesome.com/b7033fc5a6.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

        <script src="JS/Blowfish.js"></script> 
        <script src="JS/connectDB.js"></script> 

        <style>
            .fa-check {
                color: green;
            }
            .fa-times {
                color:red
            }
        </style>
    </head>
    <body>

        <%@include file ="capcalera.jsp" %>

        <h1>Sig in</h1>

        <!--<label>Username: </label><input type="text" id="name_user" name="name"><br>
        <label>Password: </label><input type="password" id="psswrd_user" name="psswrd"><br>
        <label>Confirm Password: </label><input type="password" id="c_psswrd_user" name="psswrd"><br>
        <button id="signin"> Sign in</button>
        -->

        <div class="container login mt-5">
            <article class="card-body mx-auto" style="max-width: 400px;">
                <h4 class="card-title my-4 text-center">Registro</h4>
                <form id="form" action="entrada.jsp" method="GET">
                    <input  type="text" name="op" value="sigin" hidden>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                        </div>
                        <input name="name" class="form-control" placeholder="Nombre" type="text" id="name"  required>
                        <input name="surname" class="form-control" placeholder="Apellidos" type="text" id="surnames" required>
                    </div> <!-- form-group// -->
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-birthday-cake"></i> </span>
                        </div>
                        <input name="fech_nac" class="form-control" placeholder="fecha de nacimiento" type="date" id="birthday" 
                               required>
                    </div>
                    <div class="form-group input-group mb-5">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-map-marked-alt"></i> </span>
                        </div>
                        <input name="direcc" class="form-control" placeholder="Dirección" type="text" id="adress" >
                    </div>
                    <%  Object error = session.getAttribute("sigin_error");
                        Object error_1 = session.getAttribute("sigin_error_1");
                        //Si s'ha definit la variable de error i hi ha hagut algun error mostra el missatge de error
                        if ((error != null) && ((Boolean) error)) {
                            session.setAttribute("sigin_error", false);
                    %>
                    <div class="alert alert-danger" role="alert">
                        Usuario no disponible.
                    </div>
                    <%
                    } else if ((error_1 != null) && ((Boolean) error_1)) {
                        session.setAttribute("sigin_error_1", false);
                    %>
                    <div class="alert alert-danger" role="alert">
                        Ha habido un error, intentalo más tarde.
                    </div>
                    <%
                        }
                    %>
                    <div id="usrnm">
                        <div class="form-group input-group ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-user-tag"></i> </span>
                            </div>
                            <input id="name_user" name="user_name" class="form-control" placeholder="Nombre de usuario" type="text" id="username" 
                                   required>
                        </div>
                    </div>

                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                        </div>
                        <input name="email" class="form-control" placeholder="Email" type="email" id="email" required >
                    </div> <!-- form-group// -->
                    <div class="form-group input-group">
                        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                        <input name="telef" class="form-control" placeholder="Número de teléfono" type="text" id="telephone" >
                    </div> <!-- form-group// -->

                    <div id="passwords">

                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                            </div>
                            <input onkeypress="encrypt()" id="psswrd_user" class="form-control" placeholder="Contraseña" type="password"  required>
                        </div>

                        <!-- form-group// -->
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                            </div>
                            <input id="c_psswrd_user" class="form-control" placeholder="Confirmar contraseña" type="password" 
                                   required>
                        </div>
                        <input type="password" name="psswrd_user_encr" id="psswrd_user_encr" hidden>
                    </div> 
                    </div>
                    <div class="container">
                        <article class="card-body mx-auto py-0" style="max-width: 600px;">
                            <h4 class="card-title my-2 text-center">Subscripciones</h4>

                            <table class="table table-hover text-center pb-0" >
                                <thead>
                                    <tr>
                                        <th scope="col">Subscripción</th>
                                        <th scope="col">Páginas publicas</th>
                                        <th scope="col">Página de usuario</th>
                                        <th scope="col">Página de Actores</th>
                                        <th scope="col">Precio</th>
                                        <th scope="col">Selecciona tu subscripción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">Sin permisos</th>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                        <td>0$</td>
                                        <td><input class="form-check-input" type="radio" name="lvl" id="lvl_0" value="0" disabled></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Permisos básicos</th>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                        <td>0$</td>
                                        <td><input class="form-check-input" type="radio" name="lvl" id="lvl_1" value="1" checked></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Permisos completos</th>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td>20$</td>
                                        <td><input class="form-check-input" type="radio" name="lvl" id="lvl_2" value="2" ></td>
                                    </tr>
                                </tbody>
                            </table>
                        </article>
                    </div>

                    <!-- form-group// -->
                    <div class="container login mt-2">
                        <article class="card-body mx-auto pt-0" style="max-width: 400px;">
                            <div class="form-group ">
                                <button id="sigin" class="btn btn-primary btn-block mt-4" > Crear cuenta </button>
                            </div> <!-- form-group// -->
                            <p class="text-center">Ya tienes cuenta? <a href="<%= request.getContextPath()%>/login.jsp">Entra</a> </p>
                        </article>
                    </div>
                </form>
            </article>


            <script type="text/javascript" src="JS/register.js"></script>

    </body>
</html>
