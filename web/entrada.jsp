<%-- 
    Document   : entrada
    Created on : 04-nov-2020, 17:03:34
    Author     : dmiltim
--%>

<%@page import="perbd.DBActionsRegister"%>
<%@page import="perbd.DBActionsUsers"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    </head>
    <body>

        <%
            String op = request.getParameter("op");

            if (op.equals("login")) {
                String u_name = request.getParameter("name_user");
                String password_encr = request.getParameter("psswrd_user_encr");

                String par = u_name + "-" + password_encr;
                DBActionsUsers dbau = new DBActionsUsers();
                String res = dbau.getUserAccess(par, session);
                int codigo = Integer.parseInt(res.substring(res.indexOf(":") + 1, res.length() - 1));
                switch (codigo) {
                    case 1:
                    case 2:
                        response.setStatus(response.SC_MOVED_TEMPORARILY);
                        response.setHeader("Location", request.getContextPath() + "/private/user_page.jsp");
                        break;
                    default:
                        session.setAttribute("login_error", true);
                        response.setStatus(response.SC_MOVED_TEMPORARILY);
                        response.setHeader("Location", request.getContextPath() + "/login.jsp");
                }
            } else if (op.equals("sigin")) {
                //Agafem tots els atributs que ens arriba des de la pagina de 
                //registre per guardar-ho tot. En un futur
                String name = request.getParameter("name");
                String surname = request.getParameter("surname");
                String f_nac = request.getParameter("fech_nac");
                String dir = request.getParameter("direcc");
                String u_name = request.getParameter("user_name");
                String email = request.getParameter("email");
                String tel = request.getParameter("telef");
                String password_encr = request.getParameter("psswrd_user_encr");
                String lvl = request.getParameter("lvl");

                String par = u_name + "-" + password_encr + "-" + lvl;
                DBActionsRegister dbau = new DBActionsRegister();
                String res = dbau.setUserAccess(par, session);
                int codigo = Integer.parseInt(res.substring(res.indexOf(":") + 1, res.length() - 1));
                switch (codigo) {
                    case 1:
                        response.setStatus(response.SC_MOVED_TEMPORARILY);
                        response.setHeader("Location", request.getContextPath() + "/private/user_page.jsp");
                        break;
                    case -2:
                        session.setAttribute("sigin_error", true);
                        response.setStatus(response.SC_MOVED_TEMPORARILY);
                        response.setHeader("Location", request.getContextPath() + "/register.jsp");
                        break;
                    default:
                        session.setAttribute("sigin_error", true);
                        response.setStatus(response.SC_MOVED_TEMPORARILY);
                        response.setHeader("Location", request.getContextPath() + "/register.jsp");
                }

            } else if (op.equals("update")) {
                String lvl = request.getParameter("lvl");
                DBActionsUsers dbau = new DBActionsUsers();
                String res = dbau.updateUser(lvl, session);
                switch (Integer.parseInt(res)) {
                    case -1: //Error
                        session.setAttribute("update_error", true);
                        response.setStatus(response.SC_MOVED_TEMPORARILY);
                        response.setHeader("Location", request.getContextPath() + "/private/user_page.jsp");
                        break;
                    default: //Success
                        session.setAttribute("level", Integer.parseInt(res));
                        response.setStatus(response.SC_MOVED_TEMPORARILY);
                        response.setHeader("Location", request.getContextPath() + "/index.jsp");
                        break;
                }
            } else { //log out
                session.setAttribute("user_name", null);
                session.setAttribute("level", null);
                response.setStatus(response.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", request.getContextPath() + "/index.jsp");
            }
        %>


    </body>
</html>
