<%-- 
    Document   : actor
    Created on : 27-oct-2020, 12:22:17
    Author     : dmiltim
--%>

<%@page import="perbd.DBActionsNameBasics"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Private Page</title>

        <!-- Nuvol de Paraules -->
        <script src="JS/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="JS/jquery.tagcanvas.min.js" type="text/javascript"></script>

        <!-- Pie Chart -->
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/accessibility.js"></script>
        <script src="https://code.highcharts.com/modules/pattern-fill.js"></script>
        <script src="https://code.highcharts.com/themes/high-contrast-light.js"></script>

    </head>
    <body>
        <%@include file ="../capcalera.jsp" %>
        <div class="container card-body text-center">
            <h1>Página d'un actor</h1>
        </div>

        <div class="container">
            <!-- Nuvol de paraules -->
            <div class="row">
                <div class="col">
                    <div id="myCanvasContainer">
                        <canvas width="400" height="400" id="myCanvasActors">
                            <p>In Google Chrome, things inside the canvas are inaccessible!</p>
                        </canvas>
                    </div>

                    <div id="tags">
                        <ul id="ll_autors">
                        </ul>
                    </div>
                </div>
                <div class="col" >
                    <!-- Fitza de l'actor-->
                    <div id="fitxa_actor" class="card border-dark">
                        <div  class="card-body">
                            <div class="row card-body">
                            <b>Nom: </b> <div id="nom_act"></div>
                            </div>
                            <div class="row  card-body">
                            <b>Any de neixament: </b> <div id="neix_act"></div>
                            </div>
                            <div class="row  card-body">
                            <b>Any en que va morir: </b> <div id="mor_act"></div>
                            </div>
                            <div class="row  card-body">
                            <b>Nº de pel·lícules: </b> <div id="tpelis_act"></div>
                            </div>                            
                        </div>
                    </div>
                    
                    <br>
                    
                    <!-- Pie Chart -->
                    <div id="espera" ></div>
                    <div>
                        <figure class="highcharts-figure">
                            <div id="container_actor"></div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
        <!-- Logica de la pàgina (Recollida de dades + Interacció amb l'usuari)-->
        <script src="JS/actor.js"></script>
    </body>
</html>
