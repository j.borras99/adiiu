<%-- 
    Document   : user_page
    Created on : 27-oct-2020, 12:33:08
    Author     : dmiltim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pàgina de <%= session.getAttribute("user_name")%></title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>           
        <%@include file ="../capcalera.jsp" %>

        <div class="container card-body text-center">
            <h1>Pàgina de usuari</h1>
        </div>


        <div class="container">
            <div class="row" >
                <div class="col-8 mt-3 ">
                    <div class="p-4" style="background-color: rgba(240,240,240,0.5); border-top-left-radius: 50px; border-bottom-left-radius: 50px" >
                        <div class="row ">
                            <div class="col-sm" id="image">
                                <img class="float-left img-fluid img-thumbnail" src="../imatges/defaultProfile.jpeg"></img>
                            </div>
                            <div class="col-lg">
                                <h2 id="UserName"><%= session.getAttribute("user_name")%></h2>
                                <h6 id="name">Bartomeu Ramis</h6>   
                                <br>
                                Nivel permisos: <%= session.getAttribute("level")%>
                            </div>
                            <div class="col-md">

                                <h3>Contáctame:</h3>
                                <div id="contactInfo">
                                    BartomeuRamis@gmail.com<br>
                                    +34 688 70 25 06

                                </div>

                            </div>    
                            
                        </div>
                        <br>
                        <div class="row ml-3">
                            <form action="../entrada.jsp" method="POST">
                                <input type="text" name="op" value="logout" hidden> 
                                <button class="btn btn-danger" id="logout">Log out</button>        
                            </form>
                        </div>
                    </div>
                </div>
                </br>
                <!-- Llista d'actora visitats recentment -->
                <div class="col-4">
                    <div class="card">
                        <div class="card-header">
                            Llista d'actors vists recentment
                        </div>
                        <ul class="list-group list-group-flush">
                            <div id="hist_act">    
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <form action="../entrada.jsp" method="POST">
                <input type="text" name="op" value="update" hidden>
                <div class="container">
                    <%  Object error = session.getAttribute("update_error");
                        //Si s'ha definit la variable de error i hi ha hagut algun error mostra el missatge de error
                        if ((error != null) && ((Boolean) error)) {
                            session.setAttribute("update_error", false);

                    %>
                    <div class="alert alert-danger" role="alert">
                        No se han podido actualizar los permisos.<br>
                        vuelva a intentarlo más tarde.               
                    </div>
                    <% } %>
                    <article class="card-body mx-auto py-0" style="max-width: 600px;">
                        <h4 class="card-title my-2 text-center">Subscripciones</h4>

                        <table class="table table-hover text-center pb-0" >
                            <thead>
                                <tr>
                                    <th scope="col">Subscripción</th>
                                    <th scope="col">Páginas publicas</th>
                                    <th scope="col">Página de usuario</th>
                                    <th scope="col">Página de Actores</th>
                                    <th scope="col">Precio</th>
                                    <th scope="col">Selecciona tu subscripción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">Sin permisos</th>
                                    <td><i class="fas fa-check"></i></td>
                                    <td><i class="fas fa-times"></i></td>
                                    <td><i class="fas fa-times"></i></td>
                                    <td>0$</td>
                                    <td><input class="form-check-input" type="radio" name="lvl" id="lvl_0" value="0"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Permisos básicos</th>
                                    <td><i class="fas fa-check"></i></td>
                                    <td><i class="fas fa-check"></i></td>
                                    <td><i class="fas fa-times"></i></td>
                                    <td>0$</td>
                                    <td><input class="form-check-input" type="radio" name="lvl" id="lvl_1" value="1" checked></td>
                                </tr>
                                <tr>
                                    <th scope="row">Permisos completos</th>
                                    <td><i class="fas fa-check"></i></td>
                                    <td><i class="fas fa-check"></i></td>
                                    <td><i class="fas fa-check"></i></td>
                                    <td>20$</td>
                                    <td><input class="form-check-input" type="radio" name="lvl" id="lvl_2" value="2" ></td>
                                </tr>
                            </tbody>
                        </table>
                    </article>

                </div>
                <!-- form-group// -->
                <div class="container login mt-2">
                    <article class="card-body mx-auto pt-0" style="max-width: 400px;">
                        <div class="form-group ">
                            <button id="sigin" class="btn btn-primary btn-block mt-4" > 
                                <% //if( Integer.parseInt(session.getAttribute("level").toString()) == 1){ %> 
                                Actualizar subscripción 
                            </button>
                        </div> <!-- form-group// -->                  
                    </article>
                </div>
            </form>


        </div>
    </div>
    <!-- JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>

    <script src="https://kit.fontawesome.com/b7033fc5a6.js" crossorigin="anonymous"></script>

    <!-- JavaScript propio -->
    <script type="text/javascript" src="JS/navbar.js"></script>
    <script type="text/javascript" src="JS/hoja-usuario.js"></script>
    <script src="JS/user_page.js"></script>
</body>
</html>
