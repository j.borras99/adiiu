<%-- 
    Document   : capcalera
    Created on : 06-nov-2019, 15:29:53
    Author     : dsst
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<style>
            * {box-sizing: border-box;}

            body { 
                margin: 0;
                font-family: Arial, Helvetica, sans-serif;
            }

            .header {
                overflow: hidden;
                background-color: #f1f1f1;
                padding: 20px 10px;
            }

            .header a {
                float: left;
                color: black;
                text-align: center;
                padding: 12px;
                text-decoration: none;
                font-size: 18px; 
                line-height: 25px;
                border-radius: 4px;
            }

            .header a.logo {
                font-size: 25px;
                font-weight: bold;
            }

            .header a:hover {
                background-color: #ddd;
                color: black;
            }

            .header a.active {
                background-color: dodgerblue;
                color: white;
            }

            .header-right {
                float: right;
            }

            @media screen and (max-width: 500px) {
                .header a {
                    float: none;
                    display: block;
                    text-align: left;
                }

                .header-right {
                    float: none;
                }
            }
        </style>-->

    </head>
    <body>

        <%
            String s = request.getRequestURI();
            String servletlloc = request.getContextPath();

            if (s.startsWith(servletlloc + "/private/")) {
                String usr = (String) session.getAttribute("user_name");
                //comprovam que hi hagi un usari a la sessio
                if (usr == null) {
                    response.setStatus(response.SC_MOVED_TEMPORARILY);
                    response.setHeader("Location", request.getContextPath() + "/index.jsp");
                } else {
                    int level = -1;
                    String aux = "-1";
                    if (session.getAttribute("level") != null) {
                        System.out.println("hola");
                        aux = session.getAttribute("level").toString();
                        level = Integer.parseInt(aux);
                        System.out.println("adios");
                    }
                    //Comprovam els permisos
                    //Si el nivell es 1 i intenta accedir a Actor.jsp li impedim
                    if ((level == 1) && (s.startsWith(servletlloc + "/private/actor.jsp"))) {
                        response.setStatus(response.SC_MOVED_TEMPORARILY);
                        response.setHeader("Location", request.getContextPath() + "/index.jsp");
                    } //Ens aseguram que el nivell sigui com a mínim1 per accedir a user_page.jsp 
                    else if (level < 1) {
                        response.setStatus(response.SC_MOVED_TEMPORARILY);
                        response.setHeader("Location", request.getContextPath() + "/index.jsp");
                    }
                }
            }
            
            //session.setAttribute("level", 2);
            //session.setAttribute("user_name", "admin");
        %> 

        <nav class="p-4 navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="<%= request.getContextPath()%>"><img src="<%= request.getContextPath()%>/imatges/logo.png" width="30" height="30" class="d-inline-block align-top" alt=""> UIBfilms</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto text-white">
                    <b>Date: <%= (new Date()).toString()%></b>
                </ul>
                <div class="form-inline my-2 my-lg-0">
                    <a class="active" href="<%= request.getContextPath()%>"><button class="btn btn-outline-warning mx-2 my-2 my-sm-0">Home</button></a>                   
                    <a href="<%= request.getContextPath()%>/about.jsp"><button class="btn btn-outline-warning mx-2 my-2 my-sm-0">About</button></a>
                    <a href="<%= request.getContextPath()%>/private/actor.jsp?persona=-1"><button class="btn btn-outline-warning mx-2 my-2 my-sm-0">Actors</button></a>
                    <%
                        if (session.getAttribute("user_name") != null) {
                    %>
                    <a href="<%= request.getContextPath()%>/private/user_page.jsp"> <button class="btn btn-outline-warning mx-2 my-2 my-sm-0"><%= session.getAttribute("user_name").toString()%></button> </a>
                    <%
                    } else {
                    %>
                    <a href ="<%= request.getContextPath()%>/login.jsp"><button class="btn btn-warning mx-2 my-2 my-sm-0">Log in</button></a>
                    <%
                        }
                    %>
                </div>
            </div>
        </nav>        

        <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>         
        --><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    </body>
</html>
