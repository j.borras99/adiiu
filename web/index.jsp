<%-- 
    Document   : index
    Created on : 06-nov-2019, 15:18:30
    Author     : dsst
--%>

<%@page import="perbd.DBActionsNameBasics"%>
<%@page import="perbd.DBActionsPersonaPeli"%>
<%@page import="perbd.DBActionsPeliculas"%>
<%@page import="perbd.DBActionsRatingPelis"%>
<%@page import="perbd.DBActionsPoblaciones"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap CSS -->
        <!--<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        --><title>Portal de películas</title>
    </head>
    <style>
        #grad {
            background-image: linear-gradient(180deg, rgba(220,220,220,1), rgba(255,255,255,1));
        }
    </style>
    <body id="">

        <%@include file ="capcalera.jsp" %>

        <div class="container card-body text-center" id="presentacio">    
            <h3>Benvinguts al portal de Pel·lícules</h3>
            En aquesta portal web podreu trobar informació sobre infinitat de pel·lícules, desde les primeres escenes borroses en blanc i negre fins a les més recents amb una qualitat d'imatge inmillorable. A més, en cas de subscrivir-vos podreu obtenir més informació dels actors que han participat en totes aquestes pel·lícules.
            </br>També podreu veure el ranking d'un conjunt de pel·lícules aleatories de forma gratuïta i un mape que mostrarà les ciutats Españoles són s'han gravat les pel·lícules millora valorades a nivell mundial.
        </div>
        </br></br>

        <!-- WORLD MAP -->
        <div class="px-4 container-fluid" id="map">
            <div class="row">
                <div class="col-3"></div>
                <div class="col-1">
                    <div class="row ">
                        <div class="span12 pagination-right " id="lista-poblaciones">

                        </div>       
                    </div>
                </div>
                <div class="col-6" id="container_3" style="height: 100%">       
                </div>
                     <div class="col-2"></div>
            </div>
        </div>

        <div class="container">
            <div class="text-center" id="loading"></div>
            <div id="charts" class="row">

                <!-- COLUMN DIAGRAM MOVIES-SCORE-->
                <div class="col-4">
                    <div id="container_1" >           
                    </div>
                </div>

                <!-- PIE CHART-->
                <div class="col-4">
                    <div id="container_2">           
                    </div>

                </div>

                <!-- BARS CHART-->
                <div class="col-4">
                    <div id="container_4">       
                    </div>

                </div>
            </div>
        </div>






        <!-- librerias externas-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.6/proj4.js"></script>   
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <!--<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
        -->
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/accessibility.js"></script>
        <script src="https://code.highcharts.com/modules/pattern-fill.js"></script>
        <script src="https://code.highcharts.com/themes/high-contrast-light.js"></script>
        <script src="https://code.highcharts.com/maps/modules/map.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/mapdata/countries/es/es-all.js"></script>

        <!-- js propio-->
        <script src="JS/connectDB.js"></script>
        <script type="text/javascript" src="JS/index.js"></script>

    </body>
</html>
