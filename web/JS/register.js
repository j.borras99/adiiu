/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var password = document.getElementById("psswrd_user"),
  confirm_password = document.getElementById("c_psswrd_user");
  
//Comprobación de si las contraseñas coiniden o no
function validatePassword() {
//  console.log(password.value + " =? " + confirm_password.value);
  if (document.getElementById("pswAlert") === null) {
    if (password.value !== confirm_password.value) {
      $('#passwords').prepend('<div class="alert alert-danger" id="pswAlert"> Las contraseñas no coinciden </div>');
    }
  } else if (password.value === confirm_password.value) {
    var pass = document.getElementById("pswAlert");
    if (pass !== null) {
      pass.parentNode.removeChild(pass);
    }
  }
}

function checkforcorrections() {
  var alert = document.getElementById("pswAlert");
  if (alert != null) {
    if (password.value === confirm_password.value) {
      alert.parentNode.removeChild(alert);
    }
  }
}

confirm_password.onkeyup = validatePassword;
password.onkeyup = checkforcorrections;
