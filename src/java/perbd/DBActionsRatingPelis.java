/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package perbd;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author dsst
 */
public class DBActionsRatingPelis {

    public String getPelisDeRating(String par) {
        DBConnection con = new DBConnection();
        String res = "{\"pelisderating\":";
        res += "[";
        float a = Float.parseFloat(par.substring(0, par.indexOf("-")));
        float A = Float.parseFloat(par.substring(par.indexOf("-") + 1));
        try {
            con.open();
            Statement st1, st2;
            st1 = con.getConection().createStatement();
            st2 = con.getConection().createStatement();
            String sqlq = "select * from ratingpelis where ((ratio >= " + a + ") and (ratio <= " + A + ")) limit 100;";
            ResultSet rs = st1.executeQuery(sqlq);
            String aux;
            String codpeli;
            String nompeli;
            float ratingpeli;
            while (rs.next()) {
                codpeli = rs.getString("tconst");
                ratingpeli = rs.getFloat("ratio");
                sqlq = "select * from peliculas where tconst like '" + codpeli + "';";
                ResultSet rs2 = st2.executeQuery(sqlq);
                if (rs2.next()) {
                    nompeli = rs2.getString("originaltitle");
                    aux = "";
                    aux = aux + "[\"" + nompeli + "\","+ratingpeli+" ]";
                    res = res + aux + ",";
                }
            }
            res = res.substring(0, res.length() - 1);   // quito la última coma
            res = res + "]}";
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return res;
    }
    
    public String getRatingsDePelis(String par) {
        DBConnection con = new DBConnection();
        String res = "";        
        try {
            con.open();
            Statement st1, st2;
            st1 = con.getConection().createStatement();
            st2 = con.getConection().createStatement();
            String sqlq = "select * from ratingpelis where ratingspelis.tconst = '"+par+"';";
            ResultSet rs = st1.executeQuery(sqlq);
            int votes = -1;
            float ratio = -1;
            if (rs.next()) {
                votes = rs.getInt("votes");
                ratio = rs.getFloat("ratio");              
            }

            res +=  String.valueOf(ratio) + " / " + String.valueOf(votes);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return res;
    }
    
public String getRatingsLlistaPelis(String par) {
        DBConnection con = new DBConnection();
        String res = "{ \"peliculasconrating\": [ ";
        String aux = "";
        String sqlq = "";
        try {
            con.open();
            Statement st1;
            st1 = con.getConection().createStatement();
            aux = "(";
            for (int i = 0; i < par.length(); i++) {
                if (par.charAt(i) == '"') {
                    aux += "'"+par.substring(i + 1, i + 10) + "', ";
                    i += 10;
                }
            }
            aux = aux.substring(0, aux.length() - 2);   // quito la última coma
            aux += ")";
            
            sqlq = "SELECT peliculas.originaltitle, ratingpelis.ratio, ratingpelis.votes FROM peliculas, ratingpelis WHERE peliculas.tconst = ratingpelis.tconst AND peliculas.tconst IN " + aux + ";";
            ResultSet rs = st1.executeQuery(sqlq);
            
            while (rs.next()) {
                res += "{\"title\": \""+rs.getString("originaltitle")+"\", ";            
                res += "\"rating\": "+String.valueOf(rs.getFloat("ratio"))+", ";
                res += "\"votes\": "+String.valueOf(rs.getInt("votes"))+"},";
            }
            res = res.substring(0, res.length() - 1);   // quito la última coma
            res = res + "]}";
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return sqlq;
        } finally {
            con.close();
        }
        return res;
    }

}
