package perbd;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dsst
 */
public class DBActionsRegister {

    /*
     par = "user_name"-"password"-"access_level"
     */
    public String setUserAccess(String par, HttpSession session) {
        DBConnection con = new DBConnection();
        String res = "";
        String user = par.substring(0, par.indexOf("-"));
        String aux = par.substring(par.indexOf("-") + 1);
        String passwd = aux.substring(0, aux.length() - 2);
        String lvl = "";
        int nivel = 0;
        try {
            lvl = aux.substring(aux.length() - 1);
        } catch (Exception ex) {
            res += "{\"success\":-3}";
            return res;
        }
        try {
            nivel = Integer.parseInt(lvl);
        } catch (Exception ex) {
            res += "{\"success\":" + aux + " / " + passwd + " /" + lvl + "}";
            return res;
        }
        try {
            DBActionsUsers DBAU = new DBActionsUsers();
            if (DBAU.registeredUser(user, passwd)) {
                res += "{\"success\":-2}"; //l'usuari ja esta registrat.
            } else {
                con.open();
                Statement st = con.getConection().createStatement();

                String sql = "insert into usuarios (user, passwd, nivel) VALUES ('" + user + "','" + passwd + "'," + nivel + ");";

                int rs = st.executeUpdate(sql);

                res += "{\"success\":" + rs + "}";
                session.setAttribute("user_name", user);
                session.setAttribute("level", lvl);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            res += "{\"success\":-4}";
        } finally {
            con.close();
        }
        return res;
    }
}
