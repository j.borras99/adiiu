/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package perbd;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dsst
 */
public class DBActionsUsers {

    public String getUserAccess(String par, HttpSession session) {
        DBConnection con = new DBConnection();
        String res = "";
        String user = par.substring(0, par.indexOf("-"));
        String passwd = par.substring(par.indexOf("-") + 1);
        int nivel = -1;
        try {
            con.open();
            Statement st = con.getConection().createStatement();
            String sql = "select * from usuarios where ((user='" + user + "')and(passwd='" + passwd + "'));";
            ResultSet rs = st.executeQuery(sql);
            String aux;
            
            if (rs.next()) {
                nivel = rs.getInt("nivel");                 
            }
            res = res + "{\"nivel\":" + Integer.toString(nivel) + "}";
            if(nivel >= 1){
                session.setAttribute("user_name", user);
                session.setAttribute("level", nivel);
                
                
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            res += "{\"nivel\": -1}";
        } finally {
            con.close();
        }
        return res;
    }
    
    //Codi per comprovar si hi ha algun usuari re
    public boolean registeredUser(String user, String passwd){
        DBConnection con = new DBConnection();
        boolean registered = false;
        
        try {
            con.open();
            Statement st = con.getConection().createStatement();
            String sql = "select * from usuarios where ((user='" + user + "')and(passwd='" + passwd + "'));";
            ResultSet rs = st.executeQuery(sql);
            
            if (rs.next()) {
                registered = true; 
            }         
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            con.close();
        }
        return registered;
    }
    
    //Codi per comprovar si hi ha algun usuari re
    public String updateUser(String par, HttpSession session){
        DBConnection con = new DBConnection();
        String updated = "-1";
        
        String user = session.getAttribute("user_name").toString();        
        String permisos = par;
        try {
            
            con.open();
            Statement st = con.getConection().createStatement();
            String sql = "UPDATE usuarios SET nivel = "+par+" WHERE usuarios.user = '"+user+"';";
            int rs = st.executeUpdate(sql);
            
            if(rs == 1){    //Si ha anat bé guardam a "updated" els nous permisos
                updated = par;
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return ex.toString()+" // "+updated;
        } finally {
            con.close();
        }
        return updated;
    }
}
